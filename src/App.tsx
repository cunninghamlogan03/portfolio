import React from 'react'
import './App.scss'
import Header from './components/header/Header'
import { ProjectView } from './components/projectview/ProjectView'

function App () {
  return (
    <>
      <div className='content'>
        <Header />
        <ProjectView />
      </div>
      
      {/* All modals here */}
    </>
  )
}

export default App
