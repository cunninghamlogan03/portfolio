import React from 'react';
import './Header.scss';
import {
    soteria,
    icarus,
    odyssey,
    about,
} from '../../state/slice/projectSelectSlice';
import { useAppDispatch } from '../../state/hooks';

const Header = () => {
    const dispatch = useAppDispatch();

    const setSelectedProject = (e: string) => {
        switch (e) {
            case 'projects':
                dispatch(about());
                break;
            case 'soteria':
                dispatch(soteria());
                break;
            case 'icarus':
                dispatch(icarus());
                break;
            case 'odyssey':
                dispatch(odyssey());
                break;
        }
    };

    return (
        <div className="header">
            <select
                className="projectSelectMenu"
                onChange={(e) => {
                    setSelectedProject(e.target.value);
                }}
            >
                <option value="projects">About</option>
                <option value="soteria">Soteria</option>
                <option value="icarus">Icarus</option>
                <option value="odyssey">Odyssey</option>
            </select>
        </div>
    );
};

export default Header;
