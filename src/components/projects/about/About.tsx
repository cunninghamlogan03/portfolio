import React from "react";

const About = () => {
    return (
        <div className="content-wrapper">
            My name is Logan Cunningham and I'm a software engineer!
        </div>
    )
}

export default About;