import React, { useState } from "react" 
import './Soteria.scss'
import Soteria_idf_dark from '../../../res/soteria/idf-incident-list-dark.png';
import Soteria_idf_light from '../../../res/soteria/idf-incident-list-light.png';
import Soteria_create_dark from '../../../res/soteria/create-new-incident-dark.png';
import Soteria_create_light from '../../../res/soteria/create-new-incident-light.png';
import Soteria_role_management_dark from '../../../res/soteria/user-roles-dark.png';
import Soteria_role_management_light from '../../../res/soteria/user-roles-light.png';
import Soteria_edit_incident_dark from '../../../res/soteria/edit-incident-dark.png';
import Soteria_edit_incident_light from '../../../res/soteria/edit-incident-light.png';

const images = [
    Soteria_idf_dark,
    Soteria_idf_light,
    Soteria_create_dark,
    Soteria_create_light,
    Soteria_role_management_dark,
    Soteria_role_management_light,
    Soteria_edit_incident_dark,
    Soteria_edit_incident_light,
]

const Soteria = () => {
    const [selectedImage, setSelectedImage] = useState(0);

    const renderSlides = () => {
        return (
            <div className='image-container' >
                <img src={images[selectedImage]} alt="" />
            </div>
        )
    }

    const renderDots = () => {
        return images.map((image, i) => {
            return (
                <div className="dot" key={`dot ${i.toString()}`} 
                    onClick={() => { setSelectedImage(i) }}>
                        {/* Add transition to onClick */}
                    <img src={image} alt="" />
                </div>
            )
        })
    }

    return (
        <div className='content-wrapper'>
            <span>Soteria</span>

            <div className='carousel-wrapper'>
                {renderSlides()}
                <div className='dots-wrapper'>
                    {renderDots()}
                </div>
            </div>
            <div className='description-wrapper'>
                Soteria is an application to enable Intelligence Analysts to ...
            </div>
        </div>
    )
}

export default Soteria;