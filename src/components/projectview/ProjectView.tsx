import React from "react"
import "./ProjectView.scss"
import { useAppSelector } from "../../state/hooks"
import Soteria from "../projects/soteria/Soteria"
import About from "../projects/about/About"

export const ProjectView = () => {
    const projectSelected = useAppSelector((state) => 
        state.projectSelected.projectSelected)

    const setProjectView = () => {
        switch (projectSelected) {
            case 'about':
                return <About />
            case 'soteria':
                return <Soteria />
            case 'icarus':
                return <div>Icarus</div>
            case 'odyssey':
                return <div>Odyssey</div>
        }
    }

    return (
        <div className="project-view">
            {setProjectView()}
        </div>
    )
}