import { createSlice } from '@reduxjs/toolkit';

interface ProjectSelectedState {
    projectSelected: 'soteria' | 'icarus' | 'odyssey' | 'about';
}

const initialState: ProjectSelectedState = {
    projectSelected: 'about',
};

export const projectSelectSlice = createSlice({
    name: 'projectSelect',
    initialState,
    reducers: {
        // actions
        soteria: (state) => {
            state.projectSelected = 'soteria';
        },
        icarus: (state) => {
            state.projectSelected = 'icarus';
        },
        odyssey: (state) => {
            state.projectSelected = 'odyssey';
        },
        about: (state) => {
            state.projectSelected = 'about';
        },
    },
});

export const { soteria, icarus, odyssey, about } = projectSelectSlice.actions;
export default projectSelectSlice.reducer;
