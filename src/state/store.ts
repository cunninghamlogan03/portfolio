import { configureStore } from '@reduxjs/toolkit'
import projectSelectedReducer from './slice/projectSelectSlice'

export const store = configureStore({
  reducer: {
    projectSelected: projectSelectedReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch